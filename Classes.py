import sys
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets
from generated.main_window import Ui_main_window
import sqlite3
from importlib import import_module
import importlib
import os
from os import listdir, remove
import sip
from constants import *
from functions import *
from PyQt5.QtCore import Qt
from optparse import OptionParser
import inspect


# класс работы с базой данных
class DataBase:
    def __init__(self, data_base):
        print(data_base)
        self.connection = sqlite3.connect(data_base)
        self.cursor = self.connection.cursor()

    # Получение нужных значений из таблицы
    def get(self, column_need_name, column_name='name', table='tests',
            values=['id', 'type', 'name', 'checker', 'num_questions', 'questions',
                    'general_question']):
        z = 'select ' + ', '.join(
            values) + ' from ' + table + ' where ' + column_name + ' = \'' + column_need_name + '\''
        return self.cursor.execute(z).fetchall()[0]

    # Плучене единственного значения из таблицы
    def get_one(self, column_need_name, value, column_name='name', table='tests'):
        return self.get(column_need_name, column_name, values=[value])[0]

    # Получение всей колонки
    def get_column(self, column_name, table='tests'):
        return [i[0] for i in
                self.cursor.execute('select ' + column_name + ' from ' + table).fetchall()]

    # Данный метод выполняет запрос переданный в строке s
    def exec(self, s):
        res = self.cursor.execute(s).fetchall()
        try:
            self.connection.commit()
        except BaseException:
            pass
        return res

    def load_test(self, need, load_by='name'):
        print(self.get(need, column_name=load_by))
        id, type, name, checker, num_questions, questions, general_question = self.get(need,
                                                                                       column_name=load_by)
        return Test(name=name, id=id, type=type, checker=checker, num_questions=num_questions,
                    general_question=general_question, questions=questions)

    def delete(self, need, delete_by='name'):
        z = """DELETE from tests where """ + delete_by + """= '""" + need + '\''
        self.cursor.execute(z)
        self.connection.commit()

    def insert(self, *values, with_id=False):
        if with_id:
            z = 'INSERT INTO tests VALUES (' + ', '.join(convert_for_db(values)) + ')'
        else:
            z = 'INSERT INTO tests (type, name, checker, num_questions, questions, general_question) VALUES (' + ', '.join(
                convert_for_db(values)) + ')'
        self.cursor.execute(z)
        self.connection.commit()

    def insert_test(self, test):
        if test.id == None:
            self.insert(test.type, test.name, test.checker, test.num_questions,
                        unconvert(test.questions), test.general_question)
        else:
            self.insert(test.id, test.type, test.name, test.checker, test.num_questions,
                        unconvert(test.questions), test.general_question, with_id=True)


class Test:
    def __init__(self, name=None, id=None, type=1, checker='main_checker', num_questions=None,
                 general_question='', questions=None):
        self.set_name(name)
        self.set_id(id)
        self.set_checker(checker)
        self.set_num_questions(num_questions)
        self.set_general_question(general_question)
        self.set_questions(questions)
        self.set_type(type)
        self.question_ind = -1
        self.ind = -1
        self.load_checker()

    # Занрузка кфункции чекера
    def load_checker(self):
        print('load_checker', self.checker)
        if self.checker:
            try:
                self.checker_cod = import_checker(self.checker)
            except BaseException:
                pass

    # Функции set присваивают значение если оно подходит, и обрабатывают ситуации, когда это не так
    def set_name(self, name):
        if not name:
            self.name = None
        else:
            self.name = str(name)

    def set_id(self, id):
        if not id:
            self.id = None
        else:
            ass(str(id).isdigit(), 'IdNotIntError')
            self.id = int(id)

    def set_checker(self, checker):
        self.checker = str(checker)

    def set_num_questions(self, num_questions):
        if not num_questions:
            self.num_questions = None
        else:
            ass(str(num_questions).isdigit(), 'NumQuestionsNotIntError')
            self.num_questions = num_questions

    def set_general_question(self, general_question):
        if general_question == None:
            self.general_question = ''
        else:
            self.general_question = str(general_question)

    def set_questions(self, questions):
        if not questions:
            self.questions = None
        else:
            self.questions = convert(str(questions))

    def set_type(self, type):
        ass(str(type).isdigit(), 'TypeNotIntError')
        self.type = type

    def next_question(self):
        self.ind += 1
        return self.questions[self.ind], len(self.questions) == self.ind + 1

    def __str__(self):
        return 'name={}, id={}, type={}, checker={}, num_question={}, general_question={}, questions={}'.format(
            str(self.name), str(self.id), str(self.type), str(self.checker), str(self.num_questions),
            str(self.general_question), str(self.questions))

    # Плучение типа теста ну числом, а строкой
    def type_str(self, c):
        return c.exec("""select type from types where id = """ + str(self.type))[0][0]


class MainWindow(QMainWindow, Ui_main_window):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        # Подключение стиля
        self.setStyleSheet(open("styles\\style.qss", "r").read())

        self.home_button_group.buttonClicked.connect(self.return_main)

        self.start_test_button.clicked.connect(self.choose_test)

        self.choose_test_combo_box.activated[str].connect(self.write_test)

        self.check_answer_button.clicked.connect(self.check_answer)

        self.return_main()

        self.create_test_button.clicked.connect(self.create_test)
        self.create_test_button_2.clicked.connect(self.create_test2)
        self.final_create_test_button.clicked.connect(self.create_test_final)

        self.create_checker_button.clicked.connect(self.create_checker)
        self.create_checker_final_button.clicked.connect(self.create_checker_final)
        self.choose_checker_button.clicked.connect(self.load_checker)

        self.edit_test_button.clicked.connect(self.edit_tests)
        self.remove_test_button.clicked.connect(self.start_removing_test)
        self.edit_test_button_2.clicked.connect(self.edit_test_2)

        self.edit = False
        self.edit_checker = False

        self.edit_checker_button.clicked.connect(self.edit_checker_func)

        self.delete_checker_button.clicked.connect(self.delete_checker)
        self.delete_checker_button_2.clicked.connect(self.delete_checker_2)

    # DELETE CHECKER
    def delete_checker(self):
        self.stack_widget.setCurrentIndex(8)

    def delete_checker_2(self):
        removing_checker_name = self.choose_deleting_checker_combo_box.currentText()
        msgBox = QMessageBox()
        response = msgBox.question(self, '',
                                   'Вы уверены, что хотите удалить чекер ' + removing_checker_name + '?',
                                   msgBox.Yes | msgBox.No)
        if response == msgBox.Yes:
            try:
                remove('checkers/' + removing_checker_name + '.py')
                QMessageBox.information(self, 'Уведомление',
                                        'Чекер ' + removing_checker_name + ' успешно удален.')
            except BaseException as e:
                QMessageBox.information(self, 'Ошибка',
                                        'При удалении чекера произошла ошибка ' + str(e))
                self.return_main()
            self.return_main()
        else:
            print('No')

    # EDIT CHECKER
    def edit_checker_func(self):
        self.edit_checker = True
        self.stack_widget.setCurrentIndex(6)
        self.load_checker()

    # EDIT TESTS
    def edit_tests(self):
        self.edit = True
        self.stack_widget.setCurrentIndex(7)

    def edit_test_2(self):
        self.editing_test = data_base.load_test(self.choose_edit_test_combo_box.currentText())

        self.number_of_questions_input.setText(str(self.editing_test.num_questions))
        self.new_test_name_input.setText(self.editing_test.name)
        self.general_question_input.setText(self.editing_test.general_question)
        self.new_test_type_combo_box.setCurrentText(self.editing_test.type_str(data_base))

        self.new_test_checker_combo_box.setCurrentText(self.editing_test.checker)
        self.create_test()

    def start_removing_test(self):
        removing_test_name = self.choose_edit_test_combo_box.currentText()
        msgBox = QMessageBox()
        response = msgBox.question(self, '',
                                   'Вы уверены, что хотите удалить тест ' + removing_test_name + '?',
                                   msgBox.Yes | msgBox.No)
        if response == msgBox.Yes:
            try:
                data_base.delete(removing_test_name)
            except BaseException:
                QMessageBox.information(self, 'Ошибка',
                                        'При удалении теста из базы данных произошел сбой.')
                self.return_main()
            QMessageBox.information(self, 'Уведомление',
                                    'Тест ' + removing_test_name + ' успешно удален.')
            self.return_main()

    # CREATE CHECKER
    def create_checker_final(self):
        fname = self.checker_name_edit.text()

        try:
            remove(self.last_fname)
        except BaseException:
            pass

        checkers = [i.rstrip('.py') for i in listdir('checkers')]

        fname = self.no_repeating_name(fname, checkers)

        f = open('checkers/' + fname + '.py', 'w', encoding=CHECKER_ENCODING)
        checker_text = self.enter_checker_edit.toPlainText()
        f.write(checker_text)
        f.close()

        self.return_main()

    def load_checker(self):
        try:
            fname = QFileDialog.getOpenFileName(self, 'Выбрать чекер', 'checkers')[0]
            if self.edit_checker:
                self.last_fname = fname
            self.checker_name_edit.setText(fname.split('/')[-1].split('.')[0])
            f = open(fname, encoding=CHECKER_ENCODING)
            self.enter_checker_edit.setPlainText(f.read())
            f.close()
        except BaseException:
            pass

    def create_checker(self):
        self.stack_widget.setCurrentIndex(6)

    def upd_checker_edit(self):
        self.enter_checker_edit.setPlainText(
            'def check(inp, ideal_ans, checking_ans):\n    return ideal_ans == checking_ans')

    # CREATING TEST
    def create_test(self):
        self.stack_widget.setCurrentIndex(4)

    def create_test2(self):
        type = \
            data_base.get(self.new_test_type_combo_box.currentText(), column_name='type',
                          values=['id'],
                          table='types')[0]
        name = self.new_test_name_input.text()
        num_questions = self.number_of_questions_input.text()
        general_question = self.general_question_input.text()
        checker_name = self.new_test_checker_combo_box.currentText()
        if name == '':
            QMessageBox.information(self, 'Ошибка', 'Пустое название теста')
        elif num_questions == '':
            QMessageBox.information(self, 'Ошибка', 'Не указано количество тестов')
        elif not num_questions.isdigit():
            QMessageBox.information(self, 'Ошибка', 'Количество тестов должно быть числом')
        else:
            try:
                self.new_test = Test(name=name, num_questions=num_questions,
                                     general_question=general_question,
                                     checker=checker_name, type=type)
            except Exception as e:
                QMessageBox.information(self, 'Ошибка', str(e))
                self.return_main()
                return
            self.new_test.num_questions = int(self.new_test.num_questions)
            try:
                self.create_questions_input()
                self.stack_widget.setCurrentIndex(5)
            except BaseException:
                QMessageBox.information(self, 'Ошибка', 'Произошла неизвестная приложению ошибка при создании теста')

    def create_questions_input(self):
        self.create_questions_table()

        self.fill_questions_table()

        self.questions_new_test_scroll_area.setWidgetResizable(True)
        self.questions_new_test_scroll_area.setWidget(self.questions_table)

    def create_questions_table(self):
        self.questions_table = QTableWidget(self)
        self.questions_table.setSelectionBehavior(QTableWidget.SelectRows)
        self.questions_table.setColumnCount(2)
        self.questions_table.setHorizontalHeaderLabels(['Вопрос', 'Ответ'])

    def fill_questions_table(self):
        for i in range(self.new_test.num_questions):
            self.questions_table.insertRow(i)
            question_item = QTableWidgetItem('')
            answer_item = QTableWidgetItem('')
            if self.edit and i < self.editing_test.num_questions:
                question_item = QTableWidgetItem(self.editing_test.questions[i][0])
                answer_item = QTableWidgetItem(self.editing_test.questions[i][1])
            self.questions_table.setItem(i, 0, question_item)
            self.questions_table.setItem(i, 1, answer_item)

    def create_test_final(self):
        if self.edit:
            data_base.delete(self.editing_test.name)

        self.new_test.set_name(
            self.no_repeating_name(self.new_test_name_input.text(), data_base.get_column('name')))
        self.new_test.questions = []
        for i in range(self.new_test.num_questions):
            self.new_test.questions.append(
                [self.questions_table.item(i, 0).text(), self.questions_table.item(i, 1).text()])
        data_base.insert_test(self.new_test)
        self.return_main()


    def delete_new_questions_input(self):
        if 'question_table' in [i[0] for i in inspect.getmembers(self)]:
            self.questions_table.setParent(None)

    # WRITING TEST
    def choose_test(self):
        self.stack_widget.setCurrentIndex(1)

    def write_test(self, test_name):
        self.mistakes = []
        self.right = []

        try:
            self.test = data_base.load_test(test_name)
        except Exception as e:
            QMessageBox.information(self, 'Ошибка', str(e))
            self.return_main()
            return

        self.update_questions()

        self.stack_widget.setCurrentIndex(2)

    def check_answer(self):
        checker_uncorrect = 'Чекер ' + self.test.checker + ' Некорректен. '
        try:
            self.next_question(
                self.test.checker_cod.check(self.current_question, self.current_ideal_ans,
                                            self.input_answer.text()))
        except IndexError:
            QMessageBox.information(self, 'Ошибка',
                                    checker_uncorrect + 'При проверке произошёл выход за пределы массива.')
        except KeyError:
            QMessageBox.information(self, 'Ошибка',
                                    checker_uncorrect + 'При проверке произошло обращение к несуществующему элементу объекта.')
        except StopIteration:
            QMessageBox.information(self, 'Ошибка',
                                    checker_uncorrect + 'При проверке была вызванна функция next, однако был достигнут конец итератора.')
        except ZeroDivisionError:
            QMessageBox.information(self, 'Ошибка', checker_uncorrect + 'Деление на ноль.')
        except ImportError:
            QMessageBox.information(self, 'Ошибка',
                                    checker_uncorrect + 'Обращение к несуществующему модулю.')
        except MemoryError:
            QMessageBox.information(self, 'Ошибка', checker_uncorrect + 'Недостаточно памяти.')
        except NameError:
            QMessageBox.information(self, 'Ошибка',
                                    checker_uncorrect + 'Обращение к несуществующей переменной.')
        except TypeError:
            QMessageBox.information(self, 'Ошибка',
                                    checker_uncorrect + 'Применение операции к объекту несоответствующего типа.')
        except BaseException:
            QMessageBox.information(self, 'Ошибка',
                                    checker_uncorrect + 'Произошла неизвестная приложению ошибка.')

    def next_question(self, ok):
        print('ok=', ok)
        if not ok and self.test.type == TEACHING:
            QMessageBox.information(self, 'Вы допустили ошибку',
                                    'На вопрос ' + self.current_question + ' ответ ' + self.current_ideal_ans)
        if not ok:
            if self.test.type == 1:
                self.test.questions.append(self.test.questions[0])
            self.mistakes.append(
                'Вопрос - ' + self.current_question + '\nОтвет - ' + self.current_ideal_ans + '\nВаш Ответ - ' + self.input_answer.text())
        else:
            self.right.append(
                'Вопрос - ' + self.current_question + '\nОтвет - ' + self.input_answer.text())

        self.test.questions = self.test.questions[1::]

        if len(self.test.questions):
            self.update_questions()
        else:
            self.test_result()

    def test_result(self):
        self.stack_widget.setCurrentIndex(3)
        self.result_stacked_widget.setCurrentIndex(self.test.type - 1)

        self.mistakes_label.setText('Ошибки: ' + str(len(self.mistakes)))
        self.right_answers_label.setText('Правильные ответы: ' + str(len(self.right)))

        mistakes_group = QWidget()
        right_answers_group = QWidget()

        mistakes_group.setLayout(self.fill_layout(self.mistakes))

        right_answers_group.setLayout(self.fill_layout(self.right))

        self.mist_scroll_area.setWidgetResizable(True)
        self.mist_scroll_area.setWidget(mistakes_group)
        self.right_scroll_area.setWidgetResizable(True)
        self.right_scroll_area.setWidget(right_answers_group)

    # UPDATE
    def update_questions(self):
        self.current_question = str(self.test.questions[0][0])
        self.current_ideal_ans = str(self.test.questions[0][1])

        self.question_label.setText(self.test.general_question + '\n' + self.current_question)
        self.input_answer.setText('')

    def update_all_combo_boxes(self):
        checkers = [i.rstrip('.py') for i in filter(lambda x: x.split('.')[-1] == 'py', listdir('checkers'))]
        self.set_list_to_combo_box(self.choose_test_combo_box, data_base.get_column('name'))
        self.set_list_to_combo_box(self.new_test_type_combo_box,
                                   data_base.get_column('type', table='types'))
        self.set_list_to_combo_box(self.new_test_checker_combo_box, checkers)
        self.set_list_to_combo_box(self.choose_edit_test_combo_box, data_base.get_column('name'))
        self.set_list_to_combo_box(self.choose_deleting_checker_combo_box, checkers)

    # OTHER FUNCTIONS
    # Функция return_main возвращает на главную страницу и обновляет виджеты, так как их значения
    # могли измениться
    def return_main(self):
        self.delete_new_questions_input()

        self.update_all_combo_boxes()
        self.upd_checker_edit()

        self.stack_widget.setCurrentIndex(0)

        self.new_test_name_input.setText('')
        self.number_of_questions_input.setText('')
        self.general_question_input.setText('')

    def fill_layout(self, arr):
        layout = QVBoxLayout()
        for i in arr:
            lab = QLabel(self)
            lab.setText(i)
            layout.addWidget(lab)
        return layout

    def no_repeating_name(self, curr_name, arr):
        old_name = curr_name
        if curr_name in [str(i) for i in arr]:
            curr_name += '2'
            while curr_name in arr:
                curr_name = curr_name[:-1:] + str(int(curr_name[-1]) + 1)
            QMessageBox.information(self, 'Имя ' + old_name + ' занято',
                                    'Новое имя - ' + curr_name)
        return curr_name

    def set_list_to_combo_box(self, combo_box, lst):
        lst = sorted([str(i) for i in lst])
        combo_box.clear()
        for i in lst:
            if 'pycach' not in str(i):
                combo_box.addItem(str(i))


data_base = DataBase(DATA_BASE)
TEACHING = data_base.exec ("""select id from types where type = 'teaching'""")[0][0]
CONTROL = data_base.exec ("""select id from types where type = 'control'""")[0][0]


if __name__ == "__main__":
    pass