from importlib import import_module
from constants import *


# Вызов исключения если condition = False
def ass(condition, text):
    if not condition:
        raise Exception(text)


# Получение списка тестов из базы данных
def convert(qs):
    return [i.split(SPLIT_TEST_PAUR) for i in qs.split(SPLIT_TESTS_ON_PAIRS)]


# Получение строки из списка тестов для базы данных
def unconvert(a):
    s = ''
    for i in a:
        if len(s):
            s += SPLIT_TESTS_ON_PAIRS
        s += str(i[0]) + SPLIT_TEST_PAUR + str(i[1])
    return s


def import_checker(module_name):
    return import_module('checkers.' + module_name)


# При обращении к базе данных необходимо обернуть строки в кавычки, а int привести к  str
def convert_for_db(arr):
    ret = []
    for i in arr:
        if isinstance(i, type('o')):
            ret.append('\'' + i + '\'')
        else:
            ret.append(str(i))
    return ret
