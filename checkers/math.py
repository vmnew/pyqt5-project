def check(inp, ideal_ans, checking_ans):
    d = {'sin(2a)': '2sin(a)cos(a)', 'cos(2a)': 'cos2(a)-sin2(a)=1-2sin2(a)=2cos2(a)-1',
     'tg(2a)': '2tg(a)/(1-tg2(a))', '1-cos(a)': '2sin2(a/2)', '1+cos(a)': '2cos2(a/2)',
     'sin2(a)': '(1-cos(2a))/2', 'cos2(a)': '(1+cos(2a))/2', 'tg2(a)': '(1-cos(2a))/(1+cos(2a))',
     'tg(a/2)': 'sin(a)/(1+cos(a))', 'univers sin(a)': '2tg(a/2)/(1+tg2(a/2))',
     'universe cos(a)': '(1-tg2(a/2))/(1+tg2(a/2))', 'universe tg(a)': '2tg(a/2)/(1-tg2(a/2))'}
    c = ''
    for i in checking_ans:
        if i != ' ':
            c += i
    return c != '' and c in d[inp]